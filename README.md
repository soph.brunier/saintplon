Un réseau de centre de formation (qu'on ne nommera pas) souhaite créer une plateforme unifié pour regrouper les différentes tâches courantes propres à chaque centre.

Les besoins sont les suivants : 

* Les directeurices de régions doivent pouvoir gérer les différents centres de formation 

* * Les chargé·e·s de promos doivent pouvoir gérer les promotions du ou des centre(s) auxquels ielles sont rattaché·e·s 

  * Créer des nouvelles promotions 

  * Inviter des apprenant·e·s dans les promos

  *  Valider une candidature 

  * Assigner un·e formateur·ice à une promotion 

  *  Gérer les retards et absences des apprenant·e·s *

    

    Les formateur·ice·s doivent pouvoir créer des projets puis les assigner à leurs promos ainsi que les corriger 

    On doit être capable de consulter l'agenda des formations en cours ou à venir 

    On doit pouvoir candidater à une formation à venir 

    Un·e apprenant·e doit avoir accès aux projets proposés à sa promotion ainsi que proposer un rendu. 

    elle doit également pouvoir consulter son avancement dans les compétences

Les promos sont liées à un référentiel (DWWM, CDA, TAI...) qui détermine les compétences et les projets assignables à cette promotion. Il est probable que d'autres référentiels soient rajoutés par la suite.

D'autres fonctionnalités peuvent être envisagées : Gestion des livrets d'évaluation, dossier professionnels et dossier projets via la plateforme. Gestion de l'agenda des promotions.

Objectifs En groupe de 3-4, vous devrez créer une ébauche de spécifications fonctionnelles et techniques pour ce projet. Il faudra créer les différents diagramme UML (use case, activité, classes, séquence, état-transition), faire quelques maquettes fonctionnelles.

Vous pourrez ensuite définir les sprints et les tâches (via gitlab) ainsi que définir le poids de celles ci via un planning poker.

### DIAGRAMMES DE USE CASES

#### Apprenant

![UseCaseApprenant](UseCases/Apprenant/UseCaseApprenant.png)

#### Chargé de Promotion

![UseCase-ChargePromotion](UseCases/ChargePromotion/UseCase-ChargePromotion.png)

#### Formateur

![UseCase-Formateur](UseCases/Formateur/UseCase-Formateur.png)

#### Directeur

![UseCase-Directeur](UseCases/Directeur/UseCase-Directeur.png)

#### Visiteur

![UseCase-Visiteur](UseCases/Visiteur/UseCase-Visiteur.png)



#### <u>TABLEAUX DE USE CASES</u>

##### Apprenant

![UC_VisualiserCompétences](UseCases/Apprenant/UC_VisualiserCompétences.png)

##### Chargé de Promotion

![UC_VisualiserCandidature](UseCases/ChargePromotion/UC_VisualiserCandidature.png)

##### Formateur

![UC_CorrigerProjet](UseCases/Formateur/UC_CorrigerProjet.png)

##### Directeur

![UC_SelectionnerCentreFormation](UseCases/Directeur/UC_SelectionnerCentreFormation.png)

##### Visiteur

![UC_VoirFormationsDisponibles](UseCases/Visiteur/UC_VoirFormationsDisponibles.png)

![UC_VisualiserCompétences](UseCases/Apprenant/UC_VisualiserCompétences.png)



### DIAGRAMMES D'ACTIVITE

#### Apprenant

![Apprenant-VisualiserChemin de Compétences](Diagrammes d'activité/Apprenant-VisualiserChemin de Compétences.jpg)

#### Chargé de Promotion

![ChargePromotion-Créer une promotion](Diagrammes d'activité/ChargePromotion-Créer une promotion.jpg)

![Charge de Promo-Candidature](Diagrammes d'activité/Charge de Promo-Candidature.png)Formateur

![Formateur-Correction](Diagrammes d'activité/Formateur-Correction.jpg)

![Apprenant-VisualiserChemin de Compétences](Diagrammes d'activité/Apprenant-VisualiserChemin de Compétences.jpg)

#### Chargé de Promotion

![ChargePromotion-Créer une promotion](Diagrammes d'activité/ChargePromotion-Créer une promotion.jpg)

![Charge de Promo-Candidature](Diagrammes d'activité/Charge de Promo-Candidature.png)Formateur

![Formateur-Correction](Diagrammes d'activité/Formateur-Correction.jpg)



### DIAGRAMMES DE SEQUENCE

#### Apprenant

![Apprenant-VisualiserCompétences](Diagrammes de séquence/Apprenant-VisualiserCompétences.png)

#### Charge de Promotion

![ChargePromotion-GererRetards](Diagrammes de séquence/ChargePromotion-GererRetards.png)

#### Formateur

![Formateur-AssignerReferentiel](Diagrammes de séquence/Formateur-AssignerReferentiel.png)

![Formateur-CorrigerProjet](Diagrammes de séquence/Formateur-CorrigerProjet.png)

![Apprenant-VisualiserCompétences](Diagrammes de séquence/Apprenant-VisualiserCompétences.png)

#### Charge de Promotion

![ChargePromotion-GererRetards](Diagrammes de séquence/ChargePromotion-GererRetards.png)

#### Formateur

![Formateur-AssignerReferentiel](Diagrammes de séquence/Formateur-AssignerReferentiel.png)

![Formateur-CorrigerProjet](Diagrammes de séquence/Formateur-CorrigerProjet.png)



### DIAGRAMMES DE CLASSES

#### Entités

![Entites](Diagrammes de classes/Entites.png)

#### Classes

![classes](Diagrammes de classes/classes.png)



### MAQUETTES FONCTIONNELLES

#### Apprenant : Visualiser ses compétences

![competences_mobile](Maquettes Fonctionnelles/Apprenant/competences_mobile.png)

![competences_mobile](Maquettes Fonctionnelles/Apprenant/competences_desktop.png)

#### Chargé de Promotion : Valider une candidature



![ValiderCandidature1](Maquettes Fonctionnelles/ChargePromotion/ValiderCandidature1.png)

![ValiderCandidature1-desktop](Maquettes Fonctionnelles/ChargePromotion/ValiderCandidature1-desktop.png)

![ValiderCandidature2](Maquettes Fonctionnelles/ChargePromotion/ValiderCandidature2.png)

![ValiderCandidature2-desktop](Maquettes Fonctionnelles/ChargePromotion/ValiderCandidature2-desktop.png)

![ValiderCandidature3](Maquettes Fonctionnelles/ChargePromotion/ValiderCandidature3.png)

![ValiderCandidature3-desktop](Maquettes Fonctionnelles/ChargePromotion/ValiderCandidature3-desktop.png)

#### Formateur : Corriger un projet

![CorrigerProjets](Maquettes Fonctionnelles/Formateur/CorrigerProjets.png)

![CorrigerUnProjet](Maquettes Fonctionnelles/Formateur/CorrigerUnProjet.png)

#### Formateur : Assigner un projet

![Maquette_Assigner_projet](Maquettes Fonctionnelles/Formateur/Maquette_Assigner_projet.jpg)
>>>>>>> Stashed changes
